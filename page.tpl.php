<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
 <body class="<?php print $body_classes ?>">
<div id="templatemo_container">

    <div id="templatemo_menu">
        <?php if (!empty($primary_links)): ?>
                  <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
                <?php endif; ?>
    </div> <!-- end of menu -->

    <div id="templatemo_banner">
			<?php if($logo): ?>			
			<div id="logo"><a href="<?php print base_path() ?>"><img src="<?php print $logo ?>" /></a></div>
			<?php endif; ?>
			<?php if($site_name): ?>			
			<div id="site-name"><a href="<?php print base_path() ?>"><?php print $site_name ?></a></div>
			<?php endif; ?>
    </div> <!-- end of banner -->

    <div id="templatemo_content">
    
    	<div class="margin_bottom_60"></div>
    
        <div class="section_w550 fl margin_right_40">
					<?php if($tabs || $tabs2): ?>    				
					<div class="tabs">
							<?php print $tabs ?>
							<?php print $tabs2 ?>
						</div>
					<?php endif; ?>
           <?php print $content ?>
        
        </div> <!-- end of left column -->
        
        <div class="section_w300 fl">
        
					<?php print $rightside ?>
            
        </div>
        
        <div class="cleaner"></div>
    
    </div> <!-- end of content -->

    <div id="templatemo_footer">
    
			<?php print $footer_message ?>      
        
        
    </div> <!-- end of footer -->

</div> <!-- end of container -->

</body>
<?php print $closure; ?>
</html>
